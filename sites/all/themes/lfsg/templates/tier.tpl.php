<?php if ($content): ?>
  <div<?php print $attributes; ?>>
    <div class="tier__inner">
      <h2><span><?php print $title; ?></span></h2>

      <div class="tier__threshold">
        <span class="threshold__label">No. of recommendations:</span>
        <span class="threshold__value"><?php print $tier->threshold; ?></span>
      </div>

      <div<?php print $content_attributes; ?>>
        <?php print render($content); ?>
      </div>

      <?php if (!empty($links)): ?>
        <?php print $links; ?>
      <?php endif; ?>
    </div>
  </div>
<?php endif; ?>
