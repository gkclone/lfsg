<div<?php print $attributes; ?>>
  <?php if ($display_title): ?>
    <?php print render($title_prefix); ?>
      <<?php print $title_tag . $title_attributes; ?>>
        <?php print $title; ?>
      </<?php print $title_tag; ?>>
    <?php print render($title_suffix); ?>
  <?php endif; ?>

  <div<?php print $content_attributes; ?>>
    <?php print $field_location_images ?>

    <div class="tabset">
      <ul class="nav nav--tabs tabset__nav">
        <li class="is-active"><a href="#one">Description</a></li>

        <?php if ($show_map_tab): ?>
          <li><a href="#two">Map &amp; Directions</a></li>
        <?php endif; ?>
      </ul>

      <div class="tabset__content">
        <div class="tabset__pane" id="one">
          <?php print $body; ?>

          <?php if ($facilities): ?>
          <div class="location__facilities">
            <h3>Facilities</h3>
            <ul>
              <?php foreach ($facilities as $facility): ?>
                <li><?php print $facility; ?></li>
              <?php endforeach; ?>
            </ul>
          </div>
          <?php endif; ?>

          <?php print render($location_info); ?>
        </div>

        <?php if ($show_map_tab): ?>
          <div class="tabset__pane" id="two">
            <?php print $field_location_geo; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
