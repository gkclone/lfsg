<?php if ($content): ?>
  <div<?php print $attributes; ?>>
    <?php if (!$page): ?>
      <h2><?php print $title; ?></h2>
    <?php endif; ?>

    <div<?php print $content_attributes; ?>>
      <?php print render($content); ?>
    </div>

    <?php if (!empty($links)): ?>
      <?php print $links; ?>
    <?php endif; ?>
  </div>
<?php endif; ?>
