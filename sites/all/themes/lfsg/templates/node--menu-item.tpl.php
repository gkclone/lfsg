<?php

/**
 * @file
 * Default theme implementation to display an object (node, comment, etc).
 *
 * @ingroup themeable
 */
?>
<div<?php print $attributes; ?>>
  <div class="item__info">
    <?php if (!empty($price)): ?>
      <span class="item__price"><?php print $price ?></span>
    <?php endif; ?>

    <h3 class="item__title"><?php print $title ?></h3>
  </div>
  <?php if (!empty($description)): ?>
    <p class="item__description">
      <?php print $description ?>
    </p>
  <?php endif; ?>
</div>
