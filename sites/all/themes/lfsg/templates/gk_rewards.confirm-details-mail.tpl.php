<div class="confirm-details">
  <?php if (!empty($intro)): ?>
    <table class="row prose confirm-details__intro">
      <tr>
        <td>
          <?php echo $intro ?>
        </td>
      </tr>
    </table>
  <?php endif; ?>

  <?php if (!empty($summary)): ?>
    <table class="row prose confirm-details__summary">
      <tr>
        <td>
          <?php echo $summary ?>
        </td>
      </tr>
    </table>
  <?php endif; ?>

  <table class="row prose confirm-details__cta">
    <tr>
      <td class="center">

        <table class="medium-button">
          <tr>
            <td bgcolor="#aa9e6d">
              <a href="<?php echo $confirm_details_url ?>"><?php echo $confirm_details_text ?></a>
            </td>
          </tr>
        </table>

      </td>
    </tr>
  </table>

  <?php if (!empty($outro)): ?>
    <table class="row prose confirm-details__outro">
      <tr>
        <td>
          <?php echo $outro ?>
        </td>
      </tr>
    </table>
  <?php endif; ?>
</div>
