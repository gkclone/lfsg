<div<?php print $attributes; ?>>
  <?php print $field_event_image; ?>

  <div<?php print $content_attributes; ?>>
    <?php if ($display_title): ?>
      <?php print render($title_prefix); ?>
        <<?php print $title_tag . $title_attributes; ?>>
          <?php print $title; ?>
        </<?php print $title_tag; ?>>
      <?php print render($title_suffix); ?>
    <?php endif; ?>

    <?php /*item__date icon--time*/print $field_event_date; ?>

    <?php if ($display_submitted): ?>
      <div class="<?php print $object_type; ?>__submitted">
        <?php if (!empty($permalink)): ?>
          <?php print $permalink; ?>
        <?php endif; ?>

        <?php print $submitted; ?>
      </div>
    <?php endif; ?>

    <?php print $content; ?>

    <?php if (!empty($signature)): ?>
      <div class="user-signature">
        <?php print $signature ?>
      </div>
    <?php endif; ?>

    <?php if ($view_mode == 'teaser'): ?>
      <?php print $links; ?>
      <?php !empty($readmore) ? print render($readmore) : NULL; ?>
    <?php endif; ?>

    <?php print $comments; ?>
  </div>
</div>
