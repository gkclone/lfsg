<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>
    <style type="text/css">
      <?php print $ink_css; ?>
      <?php print $custom_css; ?>
    </style>
  </head>

  <body>
    <table<?php print $attributes; ?>>
      <tr>
        <td class="center" align="center" valign="top">
          <center>

            <!-- Header start -->
            <table class="row header">
              <tr>
                <td class="center" align="center">
                  <center>

                    <table class="container">
                      <tr>
                        <td class="wrapper last">

                          <table class="twelve columns">
                            <tr>
                              <td>
                                <?php print $logo; ?>
                              </td>
                              <td class="expander"></td>
                            </tr>
                          </table>

                        </td>
                      </tr>
                    </table>

                  </center>
                </td>
              </tr>
            </table>
            <!-- Header end -->

            <!-- Content start -->
            <table class="row content">
              <tr>
                <td class="center" align="center">
                  <center>

                    <table class="container">
                      <tr>
                        <td>

                        <?php if (!empty($banner)): ?>
                          <!-- Banner start -->
                          <table class="row banner">
                            <tr>
                              <td class="wrapper last">

                                <table class="inner">
                                  <tr>
                                    <td>
                                      <?php print $banner; ?>
                                    </td>
                                    <td class="expander"></td>
                                  </tr>
                                </table>

                              </td>
                            </tr>
                          </table>
                          <!-- Banner end -->
                        <?php endif; ?>

                          <table class="row content__body">
                            <tr>
                              <td class="wrapper last">

                                <table class="inner">
                                  <tr>
                                    <td>
                                      <?php print $body; ?>
                                    </td>
                                    <td class="expander"></td>
                                  </tr>
                                </table>

                              </td>
                            </tr>
                          </table>

                        </td>
                      </tr>
                    </table>

                  </center>
                </td>
              </tr>
            </table>
            <!-- Content end -->

            <!-- Footer start -->
            <table class="row footer">
              <tr>
                <td class="center" align="center">
                  <center>

                    <table class="container">
                      <tr>
                        <td class="wrapper last">

                          <table class="twelve columns">
                            <tr>
                              <td class="center" align="center">
                                <center>
                                  <?php print $footer; ?>
                                </center>
                              </td>
                              <td class="expander"></td>
                            </tr>
                          </table>

                        </td>
                      </tr>
                    </table>

                  </center>
                </td>
              </tr>
            </table>
            <!-- Footer end -->

          </center>
        </td>
      </tr>
    </table>
  </body>
</html>
