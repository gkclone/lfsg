(function($) {
  Drupal.behaviors.lfsg = {
    attach: function (context, settings) {
      // Enable naver for main menu.
      $("#navigation .box--menu-block-gk-core-main-menu").addClass('naver').naver({ 'labelClosed' : 'Main menu' });

      // Style select form elements.
      $("select").selecter();

      // make Friends Card feedback same height
      $(".gk-cycle-slideshow-members-feedback").find(".box--block--gk-friends-card-members-feedback").equalHeights();

      // config for Friends Card feedback slideshow
      $(".gk-cycle-slideshow-members-feedback").cycle({
        slides: '.box--block--gk-friends-card-members-feedback',
        fx: 'fade',
        pauseOnHover: true,
        updateView: -1,
        speed: 1000,
        timeout: 10000,
        pagerTemplate: '<span class="icon--circle"></span>',
        pager: '.gk-cycle-slideshow__pager-controls > .gk-cycle-slideshow__pager',
        next: '.gk-cycle-slideshow__pager-controls > .gk-cycle-slideshow__next',
        prev: '.gk-cycle-slideshow__pager-controls > .gk-cycle-slideshow__prev',
        swipe: true
      });

      // add some classes that are not configured to be addable via full_html
      // wysiwyg editor
      $('.gk-cycle-slideshow__pager-controls > .gk-cycle-slideshow__next').addClass("icon--angle-right");
      $('.gk-cycle-slideshow__pager-controls > .gk-cycle-slideshow__prev').addClass("icon--angle-left");
    }
  };

  Drupal.lfsg = {};

  Drupal.lfsg.windowLoaded = function() {
    if (Modernizr.mq('only screen and (min-width: 1024px)')) {
      // Make news/events boxes on frontpage equal height.
      $(".front .box--gk-articles-gk-articles-promoted .box__content, .front .box--gk-events-gk-events-promoted .box__content").equalHeights();

      // Make menus navigation and menu items list equal heights to accomodate for sticky nav
      $(".section-menus .box--gk-menus-gk-menus-navigation, .section-menus .box--gk-menus-gk-menus-menu").equalHeights();

      // Make reward benefits block and sign-in/create account blocks the same height on sign up pages
      $(".box--gk-rewards-gk-rewards-login-create-account, .box--gk-rewards-member-benefits").equalHeights();

      // Initialise sticky nav.
      $('.section-menus .box--gk-menus-gk-menus-navigation .box__inner')
        .stick_in_parent();
    }

    // if mobile add space above 4 strip images
    if (Modernizr.mq('only screen and (max-width: 321px)')) {
      $('.box--gk-friends-card-offer:first-child > .box__content').css({"margin-top":"50px"});
    }

  }

})(jQuery);
