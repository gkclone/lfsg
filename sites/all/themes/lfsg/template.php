<?php

/**
 * Preprocess variables for html.tpl.php
 */
function lfsg_preprocess_html(&$variables, $hook) {
  // Naver library.
  drupal_add_css(libraries_get_path('naver') . '/jquery.fs.naver.css');
  drupal_add_js(libraries_get_path('naver') . '/jquery.fs.naver.min.js');

  // Selecter library.
  drupal_add_css(libraries_get_path('selecter') . '/jquery.fs.selecter.css');
  drupal_add_js(libraries_get_path('selecter') . '/jquery.fs.selecter.min.js');

  // jQuery equal heights library.
  drupal_add_js(libraries_get_path('jQuery.equalHeights') . '/jquery.equalheights.min.js');

  // jQuery sticky library.
  drupal_add_js(libraries_get_path('sticky-kit') . '/jquery.sticky-kit.min.js');

  // jQuery cycle2.
  drupal_add_js(libraries_get_path('cycle2') . '/jquery.cycle2.min.js');
}

/**
 * Process variables for html.tpl.php
 */
function lfsg_process_html(&$variables, $hook) {
  // Add less settings.
  $less_settings = less_get_settings('minima');

  drupal_add_css(drupal_get_path('theme', 'lfsg') . '/less/lfsg.less', array('less' => $less_settings));
  drupal_add_css(drupal_get_path('theme', 'lfsg') . '/less/ie.less', array('less' => $less_settings, 'browsers' => array('IE' => 'lt IE 9', '!IE' => FALSE)));

  $variables['styles'] = drupal_get_css();
}

/**
 * Preprocess variables for node.tpl.php
 */
function lfsg_preprocess_node(&$variables) {
  // Itemise these content types, useful for node listing pages.
  if (in_array($variables['type'], array('article', 'event', 'career'))) {
    $variables['content_attributes_array']['class'][] = 'item__content';

    $variables['attributes_array']['class'][] = 'items__item';
    $variables['attributes_array']['class'][] = 'items__item--' . $variables['type'];
  }

  // Add classes to the node wrapper if the node has an image field whose name
  // looks like 'field_TYPE_image'.
  if (!empty($variables['field_' . $variables['type'] . '_image'])) {
    $variables['attributes_array']['class'][] = 'has-image';

    if (!empty($variables['elements']['field_' . $variables['type'] . '_image'][0]['#image_style'])) {
      $variables['attributes_array']['class'][] = 'image-style--' . $variables['elements']['field_' . $variables['type'] . '_image'][0]['#image_style'];
    }
  }

  if ($variables['type'] == 'location' && $variables['view_mode'] == 'full') {
    // Get facilities.
    $variables['facilities'] = array();
    foreach ($variables['field_location_facilities'] as $facility) {
      $variables['facilities'][] = $facility['taxonomy_term']->name;
    }

    // Only show the 'Map & Directions' tab if we have geo information.
    if ($variables['show_map_tab'] = !empty($variables['field_location_geo'])) {
      drupal_add_js(drupal_get_path('theme', 'lfsg') . '/js/lfsg.locations.js');
    }
  }
  elseif ($variables['type'] == 'menu_item' && $variables['view_mode'] == 'teaser') {
    // Don't render menu item titles as links.
    $variables['title'] = $variables['node']->title;

    // Clear default classes.
    $variables['attributes_array']['class'] = array('menu__item');

    // Add grid cell class.
    $variables['attributes_array']['class'][] = 'grid__cell';
    $variables['attributes_array']['class'][] = 'desk--one-half';
    //$variables['attributes_array']['class'][] = 'lap--one-half'; // Uncomment if needed

    if (!empty($variables['content']['field_menu_item_price'][0]['#markup'])) {
      $variables['price'] = $variables['content']['field_menu_item_price'][0]['#markup'];
    }

    if (!empty($variables['content']['field_menu_item_description'][0]['#markup'])) {
      $variables['description'] = $variables['content']['field_menu_item_description'][0]['#markup'];
    }
  }
  elseif ($variables['type'] == 'article') {
    hide($variables['content']['field_article_image']);
    hide($variables['content']['field_article_categories']);

    unset($variables['content']['links']['comment']['#links']['comment-add']);

    if (!empty($variables['content']['links']['comment']['#links']['comment-comments'])) {
      $variables['content']['links']['comment']['#links']['comment-comments']['attributes']['class'][] = 'icon--comment';
    }

    if (!empty($variables['content']['links']['node']['#links']['node-readmore'])) {
      $variables['readmore'] = $variables['content']['links']['node'];
      $variables['readmore']['#links']['node-readmore']['attributes']['class'][] = 'icon--angle-right';

      unset($variables['content']['links']['node']);
    }
  }
  elseif ($variables['type'] == 'event') {
    hide($variables['content']['field_event_date']);
    hide($variables['content']['field_event_image']);
    hide($variables['content']['field_event_type']);
    hide($variables['content']['field_event_categories']);

    $variables['content']['field_event_date']['#theme_wrappers'] = array('container');
    $variables['content']['field_event_date']['#attributes']['class'] = array('item__date', 'icon--time');

    if (!empty($variables['content']['links']['node']['#links']['node-readmore'])) {
      $variables['readmore'] = $variables['content']['links']['node'];
      $variables['readmore']['#links']['node-readmore']['attributes']['class'][] = 'icon--angle-right';

      unset($variables['content']['links']['node']);
    }
  }
}

/**
 * Implements hook_preprocess_panels_pane().
 */
function lfsg_preprocess_panels_pane(&$variables, $hook) {
  if (!empty($variables['title'])) {
    $variables['title'] = '<span>' . $variables['title'] . '</span>';
  }

  // Apply layout classes to the panes on a menu (content and secondary menu).
  if (($node = menu_get_object()) && $node->type == 'menu') {
    switch ($variables['pane']->subtype) {
      case 'gk_menus-gk_menus_navigation':
        $variables['attributes_array']['class'][] = 'desk--one-quarter';
      break;

      case 'gk_menus-gk_menus_menu':
        $variables['attributes_array']['class'][] = 'desk--three-quarters';
      break;
    }
  }
  // Add the 'grid' layout class to the reward tiers on the referrals page.
  elseif ($variables['pane']->subtype == 'gk_rewards-gk_rewards_tiers') {
    if (lfsg_tier_count() > 1) {
      $variables['content_attributes_array']['class'][] = 'grid';
    }
  }
}

/**
 * Preprocess variables for tier.tpl.php
 */
function lfsg_preprocess_entity(&$variables) {
  // Add layout classes to the reward tiers on the referrals page.
  if ($variables['elements']['#entity_type'] == 'tier' && ($tier_count = lfsg_tier_count()) > 1) {
    static $classes = array(
      2 => 'half',
      3 => 'third',
      4 => 'quarter',
      5 => 'fifth',
    );

    if (isset($classes[$tier_count])) {
      $variables['attributes_array']['class'][] = 'grid__cell';
      $variables['attributes_array']['class'][] = 'desk--one-' . $classes[$tier_count];
    }
  }
}

/**
 * Implements hook_node_view_alter().
 */
function lfsg_node_view_alter(&$build) {
  if ($build['#bundle'] == 'career') {
    $build['apply']['#options']['attributes']['class'][] = 'icon--angle-right';
  }
}

/**
 * Implements hook_less_variables_alter().
 */
function lfsg_less_variables_alter(&$variables) {
  $variables['base-font-size'] = '13';
  $variables['base-font-colour'] = '#7c7c7c';
  $variables['base-font-family'] = 'freight-sans-pro';
  $variables['base-font-ratio'] = '1.16';

  $variables['base-gutter-width'] = '20';

  $variables['colour-primary'] = '#002663';
  $variables['colour-primary-light'] = '#a3bdda';
  $variables['colour-primary-lighter'] = '#ced5df';
  $variables['colour-primary-dark'] = '#000d23';

  $variables['colour-secondary'] = '#aa9e6d';

  $variables['colour-neutral'] = '#828282';
  $variables['colour-neutral-light'] = '#cecece';
  $variables['colour-neutral-lighter'] = '#f4f4f4';
  $variables['colour-neutral-dark'] = '#747474';
  $variables['colour-neutral-darker'] = '#515151';

  $variables['headings-font-family'] = 'futura-pt, "Century Gothic", AppleGothic, sans-serif';
}

/**
 * Implements hook_gk_rewards_user_details_form_alter().
 */
function lfsg_form_gk_rewards_user_details_form_alter(&$form, &$form_state) {
  $form['form_title'] = array(
    '#markup' => '<h2>' . t('Sign up') . '</h2>',
    '#weight' => -100,
  );
}

/**
 * Returns HTML for a wrapper for a menu sub-tree.
 */
function lfsg_menu_tree($variables) {
  return '<ul' . drupal_attributes($variables['attributes_array']) . '>' . $variables['tree'] . '</ul>';
}

/**
 * Returns HTML for a menu link and submenu.
 */
function lfsg_menu_link(array $variables) {
  $element = $variables['element'];
  $element['#localized_options']['html'] = TRUE;
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  $output = l('<span>' . $element['#title'] . '</span>', $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Implements preprocess_pane_content_header().
 */
function lfsg_preprocess_pane_content_header(&$variables) {
  // Disable breadcrumb.
  $variables['breadcrumb'] = '';

  // Hide title for menu nodes.
  if ($node = menu_get_object()) {
    if ($node->type == 'menu') {
      $variables['title'] = '';
    }
  }
}

/**
 * Implements hook_minima_layout_classes_alter().
 */
function lfsg_minima_layout_classes_alter(&$classes, $content) {
  // Layout classes
  $classes['primary_classes'] = '';
  $classes['secondary_classes'] = ' desk--one-third';
  $classes['tertiary_classes'] = ' desk--one-quarter desk--pull--three-quarters lap--one-third lap--pull--two-thirds';

  if ($content['secondary'] && $content['tertiary']) {
    $classes['primary_classes'] = ' desk--seven-twelfths';
  }
  elseif ($content['secondary'] && !$content['tertiary']) {
    $classes['primary_classes'] = ' desk--two-thirds';
  }
  elseif (!$content['secondary'] && $content['tertiary']) {
    $classes['primary_classes'] = ' desk--three-quarters desk--push--one-quarter lap--two-thirds lap--push--one-third';
  }
}

/**
 * Preprocess variables for gk-cookies-disclaimer.tpl.php
 */
function lfsg_preprocess_gk_cookies_disclaimer(&$variables) {
  // Add icon class to continue link.
  $variables['continue_link'] = l('', request_path(), array(
    'attributes' => array('class' => array('icon--double-angle-up'))
  ));
}

/**
 * Function for getting the number of tiers to appear on the referrals page in
 * the members area. We need this value in two places, hence the static var.
 */
function lfsg_tier_count() {
  static $tier_count = NULL;

  if (!isset($tier_count)) {
    $tier_count = db_select('tier')->countQuery()->execute()->fetchField();
  }

  return $tier_count;
}

/**
 * Preprocess function for the gk_locations.list-letter-group template.
 */
function lfsg_preprocess_gk_locations_list_letter_group(&$variables) {
  // Remove the locality part of the address from each locations name.
  foreach ($variables['locations'] as $location) {
    $variables['names'][$location->nid] = $location->title;
  }
}
