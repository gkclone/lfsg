<?php
/**
 * @file
 * lfsg_core.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function lfsg_core_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:lfsg_default';
  $panelizer->title = 'LFSG: Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = '';
  $panelizer->link_to_entity = FALSE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'content_header' => NULL,
      'primary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '2dc00569-36c2-4d0f-82bd-b6268123d4e9';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-95a89a33-26c1-442b-986c-58b2b32e8371';
    $pane->panel = 'content';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '95a89a33-26c1-442b-986c-58b2b32e8371';
    $display->content['new-95a89a33-26c1-442b-986c-58b2b32e8371'] = $pane;
    $display->panels['content'][0] = 'new-95a89a33-26c1-442b-986c-58b2b32e8371';
    $pane = new stdClass();
    $pane->pid = 'new-4d195ce7-4e74-4396-a187-3003a6fab45d';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4d195ce7-4e74-4396-a187-3003a6fab45d';
    $display->content['new-4d195ce7-4e74-4396-a187-3003a6fab45d'] = $pane;
    $display->panels['content_header'][0] = 'new-4d195ce7-4e74-4396-a187-3003a6fab45d';
    $pane = new stdClass();
    $pane->pid = 'new-98e4c3fe-4979-48e0-a31d-aa813c836b11';
    $pane->panel = 'tertiary';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-secondary-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '98e4c3fe-4979-48e0-a31d-aa813c836b11';
    $display->content['new-98e4c3fe-4979-48e0-a31d-aa813c836b11'] = $pane;
    $display->panels['tertiary'][0] = 'new-98e4c3fe-4979-48e0-a31d-aa813c836b11';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:lfsg_default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:lfsg_home';
  $panelizer->title = 'LFSG: Home';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = '';
  $panelizer->link_to_entity = FALSE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_home';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_header' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'search' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '7a9cd37f-41dc-47f2-a0f4-33fc6d4f0f36';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b5abfe40-da25-4640-b0b3-b57413b38f80';
    $pane->panel = 'bottom';
    $pane->type = 'block';
    $pane->subtype = 'gk_ctas-gk_ctas';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b5abfe40-da25-4640-b0b3-b57413b38f80';
    $display->content['new-b5abfe40-da25-4640-b0b3-b57413b38f80'] = $pane;
    $display->panels['bottom'][0] = 'new-b5abfe40-da25-4640-b0b3-b57413b38f80';
    $pane = new stdClass();
    $pane->pid = 'new-4a6744bc-1ce0-4a5e-af14-85beb618e865';
    $pane->panel = 'bottom';
    $pane->type = 'block';
    $pane->subtype = 'gk_articles-gk_articles_promoted';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Latest news',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '4a6744bc-1ce0-4a5e-af14-85beb618e865';
    $display->content['new-4a6744bc-1ce0-4a5e-af14-85beb618e865'] = $pane;
    $display->panels['bottom'][1] = 'new-4a6744bc-1ce0-4a5e-af14-85beb618e865';
    $pane = new stdClass();
    $pane->pid = 'new-5b1a4edb-e4bd-416d-8017-60365f1f7812';
    $pane->panel = 'bottom';
    $pane->type = 'block';
    $pane->subtype = 'gk_events-gk_events_promoted';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Upcoming events',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '5b1a4edb-e4bd-416d-8017-60365f1f7812';
    $display->content['new-5b1a4edb-e4bd-416d-8017-60365f1f7812'] = $pane;
    $display->panels['bottom'][2] = 'new-5b1a4edb-e4bd-416d-8017-60365f1f7812';
    $pane = new stdClass();
    $pane->pid = 'new-15e9ae1f-9732-4070-b20e-031daa048f32';
    $pane->panel = 'search';
    $pane->type = 'block';
    $pane->subtype = 'gk_locations-gk_locations_search_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Find a restaurant',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '15e9ae1f-9732-4070-b20e-031daa048f32';
    $display->content['new-15e9ae1f-9732-4070-b20e-031daa048f32'] = $pane;
    $display->panels['search'][0] = 'new-15e9ae1f-9732-4070-b20e-031daa048f32';
    $pane = new stdClass();
    $pane->pid = 'new-31cd25b3-3ab5-42e4-9e68-f630653cda38';
    $pane->panel = 'search';
    $pane->type = 'block';
    $pane->subtype = 'gk_menus-gk_menus_choose_location_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'View a menu',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '31cd25b3-3ab5-42e4-9e68-f630653cda38';
    $display->content['new-31cd25b3-3ab5-42e4-9e68-f630653cda38'] = $pane;
    $display->panels['search'][1] = 'new-31cd25b3-3ab5-42e4-9e68-f630653cda38';
    $pane = new stdClass();
    $pane->pid = 'new-d2afc312-bc63-41bd-a2bd-7fa71c718d52';
    $pane->panel = 'search';
    $pane->type = 'block';
    $pane->subtype = 'gk_locations-gk_locations_choose_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Book a table',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'd2afc312-bc63-41bd-a2bd-7fa71c718d52';
    $display->content['new-d2afc312-bc63-41bd-a2bd-7fa71c718d52'] = $pane;
    $display->panels['search'][2] = 'new-d2afc312-bc63-41bd-a2bd-7fa71c718d52';
    $pane = new stdClass();
    $pane->pid = 'new-b3b0ffe5-d003-4d5d-950f-5215b66947ae';
    $pane->panel = 'top';
    $pane->type = 'block';
    $pane->subtype = 'gk_banners-slideshow';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b3b0ffe5-d003-4d5d-950f-5215b66947ae';
    $display->content['new-b3b0ffe5-d003-4d5d-950f-5215b66947ae'] = $pane;
    $display->panels['top'][0] = 'new-b3b0ffe5-d003-4d5d-950f-5215b66947ae';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:lfsg_home'] = $panelizer;

  return $export;
}
