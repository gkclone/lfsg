<?php

// Plugin definition
$plugin = array(
  'title' => t('Home'),
  'category' => t('Page'),
  'icon' => 'page_home.png',
  'theme' => 'page_home',
  'css' => 'page_home.css',
  'regions' => array(
    'top' => t('Top'),
    'search' => t('Search'),
    'content_header' => t('Content header'),
    'content_top' => t('Content top'),
    'content' => t('Content'),
    'content_bottom' => t('Content bottom'),
    'bottom' => t('Bottom'),
  ),
);
