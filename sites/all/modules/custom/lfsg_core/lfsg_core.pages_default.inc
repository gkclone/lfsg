<?php
/**
 * @file
 * lfsg_core.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function lfsg_core_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'site_template_panel_lfsg_default';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -50;
  $handler->conf = array(
    'title' => 'LFSG: Default',
    'no_blocks' => 1,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'theme',
          'settings' => array(
            'theme' => 'lfsg',
          ),
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
  );
  $display = new panels_display();
  $display->layout = 'site_default';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
        'class' => '',
        'column_class' => '',
        'row_class' => '',
        'region_class' => '',
        'no_scale' => FALSE,
        'fixed_width' => '',
        'column_separation' => '0.5em',
        'region_separation' => '0.5em',
        'row_separation' => '0.5em',
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 1,
          1 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'content',
          1 => 'sidebar',
        ),
        'parent' => 'main',
        'class' => 'main-content',
      ),
      'content' => array(
        'type' => 'region',
        'title' => 'Content',
        'width' => '100',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
      'sidebar' => array(
        'type' => 'region',
        'title' => 'Sidebar',
        'width' => '200',
        'width_type' => 'px',
        'parent' => 'main-row',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'header',
        ),
        'parent' => 'main',
        'class' => 'header',
      ),
      'header' => array(
        'type' => 'region',
        'title' => 'Header',
        'width' => 100,
        'width_type' => '%',
        'parent' => '1',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
      'sidebar' => NULL,
      'header' => NULL,
      'header_top' => NULL,
      'navigation' => NULL,
      'footer' => NULL,
      'footer_bottom' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'content_header' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'd6d4df94-5f1c-41e9-9eaf-046306fd5e94';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-e5d1f15b-b30b-411b-bde8-2c9898dd4a46';
    $pane->panel = 'content';
    $pane->type = 'page_content';
    $pane->subtype = 'page_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_page_content_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e5d1f15b-b30b-411b-bde8-2c9898dd4a46';
    $display->content['new-e5d1f15b-b30b-411b-bde8-2c9898dd4a46'] = $pane;
    $display->panels['content'][0] = 'new-e5d1f15b-b30b-411b-bde8-2c9898dd4a46';
    $pane = new stdClass();
    $pane->pid = 'new-4dcc78d9-b395-4129-a7e4-c224eb501d80';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4dcc78d9-b395-4129-a7e4-c224eb501d80';
    $display->content['new-4dcc78d9-b395-4129-a7e4-c224eb501d80'] = $pane;
    $display->panels['content_header'][0] = 'new-4dcc78d9-b395-4129-a7e4-c224eb501d80';
    $pane = new stdClass();
    $pane->pid = 'new-54245190-bd86-4b9c-9e6d-e3b43f9a926c';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Pages',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '54245190-bd86-4b9c-9e6d-e3b43f9a926c';
    $display->content['new-54245190-bd86-4b9c-9e6d-e3b43f9a926c'] = $pane;
    $display->panels['footer'][0] = 'new-54245190-bd86-4b9c-9e6d-e3b43f9a926c';
    $pane = new stdClass();
    $pane->pid = 'new-a0e7e471-a8f2-41de-9203-e13db8113bf4';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'lfsg_core-lfsg_core_affiliate_links';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'a0e7e471-a8f2-41de-9203-e13db8113bf4';
    $display->content['new-a0e7e471-a8f2-41de-9203-e13db8113bf4'] = $pane;
    $display->panels['footer'][1] = 'new-a0e7e471-a8f2-41de-9203-e13db8113bf4';
    $pane = new stdClass();
    $pane->pid = 'new-fba12c09-30ea-4640-a9e2-4910b8aa7dcc';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'gk_social-gk_social_networks';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Follow us',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'fba12c09-30ea-4640-a9e2-4910b8aa7dcc';
    $display->content['new-fba12c09-30ea-4640-a9e2-4910b8aa7dcc'] = $pane;
    $display->panels['footer'][2] = 'new-fba12c09-30ea-4640-a9e2-4910b8aa7dcc';
    $pane = new stdClass();
    $pane->pid = 'new-9a4d241f-fdf5-42d5-b8d4-7cb09dce730a';
    $pane->panel = 'footer_bottom';
    $pane->type = 'block';
    $pane->subtype = 'gk_core-copyright';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9a4d241f-fdf5-42d5-b8d4-7cb09dce730a';
    $display->content['new-9a4d241f-fdf5-42d5-b8d4-7cb09dce730a'] = $pane;
    $display->panels['footer_bottom'][0] = 'new-9a4d241f-fdf5-42d5-b8d4-7cb09dce730a';
    $pane = new stdClass();
    $pane->pid = 'new-91ec6b55-0342-4b49-9912-8b31a20484db';
    $pane->panel = 'footer_bottom';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-footer-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '91ec6b55-0342-4b49-9912-8b31a20484db';
    $display->content['new-91ec6b55-0342-4b49-9912-8b31a20484db'] = $pane;
    $display->panels['footer_bottom'][1] = 'new-91ec6b55-0342-4b49-9912-8b31a20484db';
    $pane = new stdClass();
    $pane->pid = 'new-8372bce6-b8c5-4b4d-9fe0-7a6a3089db15';
    $pane->panel = 'header';
    $pane->type = 'pane_header';
    $pane->subtype = 'pane_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = '';
    $pane->uuid = '8372bce6-b8c5-4b4d-9fe0-7a6a3089db15';
    $display->content['new-8372bce6-b8c5-4b4d-9fe0-7a6a3089db15'] = $pane;
    $display->panels['header'][0] = 'new-8372bce6-b8c5-4b4d-9fe0-7a6a3089db15';
    $pane = new stdClass();
    $pane->pid = 'new-3acf7724-b47c-4156-8ae2-dcbfb54b51fe';
    $pane->panel = 'header';
    $pane->type = 'block';
    $pane->subtype = 'gk_members-gk_members_signup_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Sign up for offers',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '3acf7724-b47c-4156-8ae2-dcbfb54b51fe';
    $display->content['new-3acf7724-b47c-4156-8ae2-dcbfb54b51fe'] = $pane;
    $display->panels['header'][1] = 'new-3acf7724-b47c-4156-8ae2-dcbfb54b51fe';
    $pane = new stdClass();
    $pane->pid = 'new-14fa90e9-ad2a-4eb0-91f3-eaba8f48bdf8';
    $pane->panel = 'header';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-user-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '14fa90e9-ad2a-4eb0-91f3-eaba8f48bdf8';
    $display->content['new-14fa90e9-ad2a-4eb0-91f3-eaba8f48bdf8'] = $pane;
    $display->panels['header'][2] = 'new-14fa90e9-ad2a-4eb0-91f3-eaba8f48bdf8';
    $pane = new stdClass();
    $pane->pid = 'new-a5b26d14-677f-4dd8-8203-d345f8c035fc';
    $pane->panel = 'navigation';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a5b26d14-677f-4dd8-8203-d345f8c035fc';
    $display->content['new-a5b26d14-677f-4dd8-8203-d345f8c035fc'] = $pane;
    $display->panels['navigation'][0] = 'new-a5b26d14-677f-4dd8-8203-d345f8c035fc';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-91ec6b55-0342-4b49-9912-8b31a20484db';
  $handler->conf['display'] = $display;
  $export['site_template_panel_lfsg_default'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'site_template_panel_lfsg_panels';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -60;
  $handler->conf = array(
    'title' => 'LFSG: Panels',
    'no_blocks' => 1,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'entity_is_panelized',
          'settings' => NULL,
          'context' => array(
            0 => 'node',
            1 => 'account',
            2 => 'term',
          ),
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'theme',
          'settings' => array(
            'theme' => 'lfsg',
          ),
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
  );
  $display = new panels_display();
  $display->layout = 'site_panels';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
        'class' => '',
        'column_class' => '',
        'row_class' => '',
        'region_class' => '',
        'no_scale' => FALSE,
        'fixed_width' => '',
        'column_separation' => '0.5em',
        'region_separation' => '0.5em',
        'row_separation' => '0.5em',
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 1,
          1 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'content',
          1 => 'sidebar',
        ),
        'parent' => 'main',
        'class' => 'main-content',
      ),
      'content' => array(
        'type' => 'region',
        'title' => 'Content',
        'width' => '100',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
      'sidebar' => array(
        'type' => 'region',
        'title' => 'Sidebar',
        'width' => '200',
        'width_type' => 'px',
        'parent' => 'main-row',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'header',
        ),
        'parent' => 'main',
        'class' => 'header',
      ),
      'header' => array(
        'type' => 'region',
        'title' => 'Header',
        'width' => 100,
        'width_type' => '%',
        'parent' => '1',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
      'sidebar' => NULL,
      'header' => NULL,
      'header_top' => NULL,
      'navigation' => NULL,
      'footer' => NULL,
      'footer_bottom' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '99215fd8-96f1-40f5-b9e3-bfc459fe20c6';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-424c13e8-bee8-4711-89ae-97478ca094be';
    $pane->panel = 'content';
    $pane->type = 'page_content';
    $pane->subtype = 'page_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_page_content_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '424c13e8-bee8-4711-89ae-97478ca094be';
    $display->content['new-424c13e8-bee8-4711-89ae-97478ca094be'] = $pane;
    $display->panels['content'][0] = 'new-424c13e8-bee8-4711-89ae-97478ca094be';
    $pane = new stdClass();
    $pane->pid = 'new-2484149f-b7d3-4620-bf9f-5753ad447c40';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Pages',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2484149f-b7d3-4620-bf9f-5753ad447c40';
    $display->content['new-2484149f-b7d3-4620-bf9f-5753ad447c40'] = $pane;
    $display->panels['footer'][0] = 'new-2484149f-b7d3-4620-bf9f-5753ad447c40';
    $pane = new stdClass();
    $pane->pid = 'new-9bfa4bda-9967-4512-9eb6-416ec270cf28';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'lfsg_core-lfsg_core_affiliate_links';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '9bfa4bda-9967-4512-9eb6-416ec270cf28';
    $display->content['new-9bfa4bda-9967-4512-9eb6-416ec270cf28'] = $pane;
    $display->panels['footer'][1] = 'new-9bfa4bda-9967-4512-9eb6-416ec270cf28';
    $pane = new stdClass();
    $pane->pid = 'new-78575380-5231-4074-8350-6012bff10e57';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'gk_social-gk_social_networks';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Follow us',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '78575380-5231-4074-8350-6012bff10e57';
    $display->content['new-78575380-5231-4074-8350-6012bff10e57'] = $pane;
    $display->panels['footer'][2] = 'new-78575380-5231-4074-8350-6012bff10e57';
    $pane = new stdClass();
    $pane->pid = 'new-914a9974-9e5a-41f5-a9e0-0362163fb196';
    $pane->panel = 'footer_bottom';
    $pane->type = 'block';
    $pane->subtype = 'gk_core-copyright';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '914a9974-9e5a-41f5-a9e0-0362163fb196';
    $display->content['new-914a9974-9e5a-41f5-a9e0-0362163fb196'] = $pane;
    $display->panels['footer_bottom'][0] = 'new-914a9974-9e5a-41f5-a9e0-0362163fb196';
    $pane = new stdClass();
    $pane->pid = 'new-c89036f4-3db1-4333-928d-84ef294994ab';
    $pane->panel = 'footer_bottom';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-footer-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c89036f4-3db1-4333-928d-84ef294994ab';
    $display->content['new-c89036f4-3db1-4333-928d-84ef294994ab'] = $pane;
    $display->panels['footer_bottom'][1] = 'new-c89036f4-3db1-4333-928d-84ef294994ab';
    $pane = new stdClass();
    $pane->pid = 'new-cb086c42-1800-4d93-9cad-96140de68574';
    $pane->panel = 'header';
    $pane->type = 'pane_header';
    $pane->subtype = 'pane_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = '';
    $pane->uuid = 'cb086c42-1800-4d93-9cad-96140de68574';
    $display->content['new-cb086c42-1800-4d93-9cad-96140de68574'] = $pane;
    $display->panels['header'][0] = 'new-cb086c42-1800-4d93-9cad-96140de68574';
    $pane = new stdClass();
    $pane->pid = 'new-ff81c3fd-14de-4131-bbfa-5c4ea8e297ba';
    $pane->panel = 'header';
    $pane->type = 'block';
    $pane->subtype = 'gk_members-gk_members_signup_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Sign up for offers',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'ff81c3fd-14de-4131-bbfa-5c4ea8e297ba';
    $display->content['new-ff81c3fd-14de-4131-bbfa-5c4ea8e297ba'] = $pane;
    $display->panels['header'][1] = 'new-ff81c3fd-14de-4131-bbfa-5c4ea8e297ba';
    $pane = new stdClass();
    $pane->pid = 'new-cb870221-862c-4c4f-a39f-873d45dbafe5';
    $pane->panel = 'header';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-user-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'cb870221-862c-4c4f-a39f-873d45dbafe5';
    $display->content['new-cb870221-862c-4c4f-a39f-873d45dbafe5'] = $pane;
    $display->panels['header'][2] = 'new-cb870221-862c-4c4f-a39f-873d45dbafe5';
    $pane = new stdClass();
    $pane->pid = 'new-9b158416-bbbe-4d1f-9aec-75200c35b8f4';
    $pane->panel = 'navigation';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9b158416-bbbe-4d1f-9aec-75200c35b8f4';
    $display->content['new-9b158416-bbbe-4d1f-9aec-75200c35b8f4'] = $pane;
    $display->panels['navigation'][0] = 'new-9b158416-bbbe-4d1f-9aec-75200c35b8f4';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-c89036f4-3db1-4333-928d-84ef294994ab';
  $handler->conf['display'] = $display;
  $export['site_template_panel_lfsg_panels'] = $handler;

  return $export;
}
