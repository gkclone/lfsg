<?php

/**
 * @file
 * Migrate vouchers to rewards with reward type as voucher.
 * Migrate voucher signups and related users to new rewards module tables
 */

abstract class RewardsMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->team = array(
      new MigrateTeamMember('Olly Nevard', 'olivernevard@greeneking.co.uk', t('Lead developer')),
      new MigrateTeamMember('Brendan Hurley', 'brendanhurley@greeneking.co.uk', t('Junior developer')),
    );
  }
}

class VoucherSignupsMigration extends RewardsMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Migrate voucher signups data to reward');
    $this->dependencies = array('VoucherNodes');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'sid' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'Voucher Signup ID.',
          'alias' => 'vs',
        ),
      ),
      MigrateDestinationTable::getKeySchema('gk_rewards_fulfilments')
    );

    $query = db_select('gk_vouchers_signups', 'vs');
    $query->innerJoin('users', 'u', "u.uid = vs.uid");
    $query->leftJoin('field_data_field_user_comms_opt_in_brand', 'cob', "cob.entity_id = u.uid and cob.entity_type = 'user'");
    $query->leftJoin('field_data_field_user_comms_opt_in_other', 'coo', "coo.entity_id = u.uid and coo.entity_type = 'user'");
    $query->leftJoin('field_data_field_user_date_of_birth', 'dob', "dob.entity_id = u.uid and dob.entity_type = 'user'");
    $query->leftJoin('field_data_field_user_first_name', 'fn', "fn.entity_id = u.uid and fn.entity_type = 'user'");
    $query->leftJoin('field_data_field_user_last_name', 'ln', "ln.entity_id = u.uid and ln.entity_type = 'user'");
    $query->leftJoin('field_data_field_user_postcode', 'pc', "pc.entity_id = u.uid and pc.entity_type = 'user'");
    $query->leftJoin('field_data_field_user_title', 't', "t.entity_id = u.uid and t.entity_type = 'user'");

    $query->fields('vs', array('sid', 'voucher_nid', 'voucher_code', 'confirmed', 'created', 'changed'));
    $query->fields('u', array('mail'));

    $query->addField('cob', 'field_user_comms_opt_in_brand_value', 'comms_opt_in_brand');
    $query->addField('coo', 'field_user_comms_opt_in_other_value', 'comms_opt_in_other');
    $query->addField('dob', 'field_user_date_of_birth_value', 'date_of_birth');
    $query->addField('fn', 'field_user_first_name_value', 'first_name');
    $query->addField('ln', 'field_user_last_name_value', 'last_name');
    $query->addField('pc', 'field_user_postcode_value', 'postcode');
    $query->addField('t', 'field_user_title_value', 'title');

    $query->orderBy('sid', 'ASC');

    $this->source = new MigrateSourceSQL($query);

    $this->destination = new MigrateDestinationTable('gk_rewards_fulfilments');

    $this->addFieldMapping('rid', 'voucher_nid')
         ->sourceMigration('VoucherNodes');

    $this->addFieldMapping('uid')
         ->defaultValue(0);

    $this->addFieldMapping('code', 'voucher_code');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('changed', 'changed');
    $this->addFieldMapping('status', 'confirmed');

    // Unmigrated source fields.
    $this->addUnmigratedSources(array(
      'mail',
      'comms_opt_in_brand',
      'comms_opt_in_other',
      'date_of_birth',
      'first_name',
      'last_name',
      'postcode',
      'title',
    ));

    // Unmigrated destination fields.
    $this->addUnmigratedDestinations(array(
      'rfid',
    ));
  }

  public function prepare($entity, stdClass $source_row) {
    $entity->status = empty($source_row->confirmed);
  }

  public function complete($entity, stdClass $source_row) {
    $entity->type = 'voucher_reward';

    $user_details_row = new stdClass();

    $user_details_row->rfid = $entity->rfid;
    $user_details_row->mail = $source_row->mail;
    $user_details_row->title = $source_row->title;
    $user_details_row->first_name = $source_row->first_name;
    $user_details_row->last_name = $source_row->last_name;
    $user_details_row->postcode = $source_row->postcode;
    $user_details_row->date_of_birth = date('Y-m-d', strtotime($source_row->date_of_birth));
    $user_details_row->comms_opt_in_brand = !$source_row->comms_opt_in_brand;
    $user_details_row->comms_opt_in_other = $source_row->comms_opt_in_other;
    $user_details_row->created = time();
    $user_details_row->changed = REQUEST_TIME;
    $user_details_row->hash = gk_core_hash($user_details_row->mail . $user_details_row->rfid . REQUEST_TIME);

    $status = drupal_write_record('gk_rewards_user_details', $user_details_row);

    if ($source_row->confirmed > 1) {
      for ($i = 2; $i <= $source_row->confirmed; $i++) {
        $new_entity = clone($entity);
        unset($new_entity->rfid);
        unset($user_details_row->rfid);
        unset($user_details_row->udid);

        $status = drupal_write_record('gk_rewards_fulfilments', $new_entity);
        $user_details_row->rfid = $new_entity->rfid;
        $status = drupal_write_record('gk_rewards_user_details', $user_details_row);
      }
    }
  }
}

class VoucherNodesMigration extends RewardsMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Migrate voucher nodes to reward entities of type voucher.');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'nid' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'Node ID.',
          'alias' => 'n',
        ),
      ),
      MigrateDestinationEntityAPI::getKeySchema('reward')
    );

    $query = db_select('node', 'n');
    $query->leftJoin('field_data_field_voucher_offer', 'vo', "vo.revision_id = n.vid AND vo.entity_type = 'node'");
    $query->leftJoin('field_data_field_voucher_summary', 'vs', "vs.revision_id = n.vid AND vs.entity_type = 'node'");
    $query->leftJoin('field_data_field_voucher_terms_conditions', 'tc', "tc.revision_id = n.vid AND tc.entity_type = 'node'");
    $query->leftJoin('field_data_body', 'b', "b.revision_id = n.vid AND b.entity_type = 'node'");
    $query->fields('n', array('nid', 'title', 'uid', 'status', 'created', 'changed'));
    $query->addField('vo', 'field_voucher_offer_value', 'offer');
    $query->addField('vs', 'field_voucher_summary_value', 'summary');
    $query->addField('tc', 'field_voucher_terms_conditions_value', 'tsandcs');
    $query->addField('b', 'body_value', 'body');
    $query->condition('n.type', 'voucher');
    $query->orderBy('nid', 'ASC');

    $this->source = new MigrateSourceSQL($query);

    $this->destination = new MigrateDestinationEntityAPI('reward', 'voucher_reward');

    $this->addFieldMapping('uid', 'uid');
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('changed', 'changed');
    $this->addFieldMapping('status', 'status');

    $this->addFieldMapping('public')
         ->defaultValue(1);

    $this->addFieldMapping('type')
         ->defaultValue('voucher_reward');

    $this->addFieldMapping('field_voucher_reward_offer', 'offer');
    $this->addFieldMapping('field_voucher_reward_summary', 'summary');
    $this->addFieldMapping('field_voucher_reward_tsandcs', 'tsandcs');
    $this->addFieldMapping('field_reward_body', 'body');

    $this->addFieldMapping('field_reward_body:format')
         ->defaultValue('filtered_html');

    // Unmigrated destination fields.
    $this->addUnmigratedDestinations(array(
      'rid',
      'url',
      'field_voucher_reward_offer:language',
      'field_voucher_reward_summary:language',
      'field_voucher_reward_tsandcs:language',
      'field_reward_body:summary',
      'field_reward_body:language',
      'path',
    ));
  }
}

/**
 *
 */
class DotMailerCSVMigration extends RewardsMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->description =
      t('Migrate GK Rewards User Details from a DotMailer exported CSV');

    // Define CSV columns.
    $columns = array(
      array('Created', 'Created'),
      array('Modified', 'Changed'),
      array('Email', 'Email'),
      array('EmailType', 'Email Type'),
      array('OptInType', 'Opt-in Type'),
      array('Lastmaileddate', 'Last mailed date'),
      array('Custno', 'Cust no (id)'),
      array('Customertype', 'Customer type'),
      array('Dob', 'Date of birth'),
      array('Firstname', 'First name'),
      array('Fullname', 'Full name'),
      array('Gender', 'Gender'),
      array('Lastname', 'Last name'),
      array('Lfstorename', 'LFSG store name'),
      array('Lfstorenumber', 'LFSG store number'),
      array('Loyaltyp1', 'Loyalty 1'),
      array('Loyaltyp2', 'Loyalty 2'),
      array('Postcode', 'Postcode'),
      array('Title', 'Title'),
    );

    $path = drupal_get_path('module', 'lfsg_rewards') . '/csv/dotmailer_exported.csv';

    // Define CSV options.
    $options = array(
      'header_rows' => 1,
      'embedded_newlines' => TRUE,
    );

    // Define source and destination.
    $this->source = new MigrateSourceCSV($path, $columns, $options);
    $this->destination = new MigrateDestinationTable('gk_rewards_user_details');

    // Define map.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'Email' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'User email address',
        ),
      ),
      MigrateDestinationTable::getKeySchema('gk_rewards_user_details')
    );

    // Add field mappings.
    $this->addFieldMapping('rfid')
         ->defaultValue(0);
    $this->addFieldMapping('mail', 'Email');
    $this->addFieldMapping('title', 'Title');
    $this->addFieldMapping('first_name', 'Firstname');
    $this->addFieldMapping('last_name', 'Lastname');
    $this->addFieldMapping('postcode', 'Postcode');
    $this->addFieldMapping('date_of_birth', 'Dob');
    $this->addFieldMapping('comms_opt_in_brand')
         ->defaultValue(1);
    $this->addFieldMapping('comms_opt_in_other')
         ->defaultValue(0);
    $this->addFieldMapping('created')
         ->defaultValue(time());
    $this->addFieldMapping('changed')
         ->defaultValue(REQUEST_TIME);
    $this->addFieldMapping('hash')
         ->defaultValue(1); // Will be calculated in prepare;

    // Unmigrated source fields.
    $this->addUnmigratedSources(array(
      'Created',
      'Modified',
      'EmailType',
      'OptInType',
      'Lastmaileddate',
      'Custno',
      'Customertype',
      'Fullname',
      'Gender',
      'Lfstorename',
      'Lfstorenumber',
      'Loyaltyp1',
      'Loyaltyp2',
    ));

    // Unmigrated source fields.
    $this->addUnmigratedDestinations(array(
      'udid',
      'data',
    ));
  }

  public function prepareRow($row) {
    // Date of birth
    if (strlen($row->Dob) >= 10 && $dob = date_create_from_format('d/m/Y', substr($row->Dob, 0, 10))) {
      $row->Dob = $dob->format('Y-m-d');
    }
    else {
      $row->Dob = NULL;
    }
  }

  public function prepare($entity, stdClass $source_row) {
    $entity->hash = gk_core_hash($entity->mail . REQUEST_TIME);
  }
}

/**
 * Implements hook_migrate_api().
 */
function lfsg_rewards_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'lfsg_rewards' => array(
        'title' => t('LFSG Rewards Migrations'),
      ),
    ),
    'migrations' => array(
      'VoucherNodes' => array(
        'class_name' => 'VoucherNodesMigration',
        'group_name' => 'lfsg_rewards',
      ),
      'VoucherSignups' => array(
        'class_name' => 'VoucherSignupsMigration',
        'group_name' => 'lfsg_rewards',
      ),
      'DotMailerCSV' => array(
        'class_name' => 'DotMailerCSVMigration',
        'group_name' => 'lfsg_rewards',
      ),
    ),
  );
  return $api;
}
