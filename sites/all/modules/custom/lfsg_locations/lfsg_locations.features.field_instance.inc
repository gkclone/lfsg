<?php
/**
 * @file
 * lfsg_locations.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function lfsg_locations_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'user-user-field_user_location'
  $field_instances['user-user-field_user_location'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 8,
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'field_user_location',
    'label' => 'Local Loch Fyne restaurant',
    'required' => 0,
    'settings' => array(
      'user_register_form' => 1,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Local Loch Fyne restaurant');

  return $field_instances;
}
