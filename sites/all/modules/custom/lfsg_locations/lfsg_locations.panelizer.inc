<?php
/**
 * @file
 * lfsg_locations.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function lfsg_locations_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:location:lfsg_default';
  $panelizer->title = 'LFSG: Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'location';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_header' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'bc5e2e41-9444-4b8c-8e85-0991717befe8';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-e1434193-0e50-45b4-bda5-9e3bfe915125';
    $pane->panel = 'content';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e1434193-0e50-45b4-bda5-9e3bfe915125';
    $display->content['new-e1434193-0e50-45b4-bda5-9e3bfe915125'] = $pane;
    $display->panels['content'][0] = 'new-e1434193-0e50-45b4-bda5-9e3bfe915125';
    $pane = new stdClass();
    $pane->pid = 'new-605ef491-498e-45ab-a9de-e0ed106331cd';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '605ef491-498e-45ab-a9de-e0ed106331cd';
    $display->content['new-605ef491-498e-45ab-a9de-e0ed106331cd'] = $pane;
    $display->panels['content_header'][0] = 'new-605ef491-498e-45ab-a9de-e0ed106331cd';
    $pane = new stdClass();
    $pane->pid = 'new-8ea720e7-82d1-4157-a310-d40320d7555e';
    $pane->panel = 'secondary';
    $pane->type = 'block';
    $pane->subtype = 'gk_livebookings-gk_livebookings_booktable';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '8ea720e7-82d1-4157-a310-d40320d7555e';
    $display->content['new-8ea720e7-82d1-4157-a310-d40320d7555e'] = $pane;
    $display->panels['secondary'][0] = 'new-8ea720e7-82d1-4157-a310-d40320d7555e';
    $pane = new stdClass();
    $pane->pid = 'new-04938f96-c017-4cd3-9898-28bc79f7becf';
    $pane->panel = 'secondary';
    $pane->type = 'block';
    $pane->subtype = 'gk_menus-gk_menus_navigation';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Menus',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '04938f96-c017-4cd3-9898-28bc79f7becf';
    $display->content['new-04938f96-c017-4cd3-9898-28bc79f7becf'] = $pane;
    $display->panels['secondary'][1] = 'new-04938f96-c017-4cd3-9898-28bc79f7becf';
    $pane = new stdClass();
    $pane->pid = 'new-4e8db927-1f50-40e1-9f38-f7a6f14edb4c';
    $pane->panel = 'tertiary';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-secondary-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => 'Menus',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4e8db927-1f50-40e1-9f38-f7a6f14edb4c';
    $display->content['new-4e8db927-1f50-40e1-9f38-f7a6f14edb4c'] = $pane;
    $display->panels['tertiary'][0] = 'new-4e8db927-1f50-40e1-9f38-f7a6f14edb4c';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:location:lfsg_default'] = $panelizer;

  return $export;
}
