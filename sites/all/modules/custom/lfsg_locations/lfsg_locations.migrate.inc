<?php

/**
 * @file
 * Because the name of this file is the module name plus '.migrate.inc', when
 * hook_migrate_api is invoked by the Migrate module this file is automatically
 * loaded - thus, you don't need to implement your hook in the .module file.
 */

/**
 * Implements hook_migrate_api().
 */
function lfsg_locations_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'location' => array(
        'title' => t('Location Imports'),
      ),
    ),
    'migrations' => array(
      'LocationNode' => array(
        'class_name' => 'LocationNodeMigration',
        'group_name' => 'location',
      ),
    ),
  );
  return $api;
}
