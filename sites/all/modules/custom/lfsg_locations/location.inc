<?php

/**
 *
 */
abstract class LocationMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->team = array(
      new MigrateTeamMember('Olly Nevard', 'olivernevard@greeneking.com',
                            t('Lead UI Developer')),
      new MigrateTeamMember('Tom Cant', 'tomcant@greeneking.co.uk',
                            t('Lead Backend Developer')),
    );

    //$this->issuePattern = 'http://drupal.org/node/:id:';
  }
}

/**
 *
 */
class LocationNodeMigration extends LocationMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->description =
      t('Migrate locations from a CSV to nodes');

    // Define CSV columns.
    $columns = array(
      array('id', 'ID'),
      array('title', 'Title'),
      array('address_1', 'Address 1'),
      array('address_2', 'Address 2'),
      array('town', 'Town'),
      array('county', 'County'),
      array('postcode', 'Postcode'),
      array('telephone', 'Telephone'),
      array('fax', 'Fax'),
      array('email', 'Email'),
      array('opening_hours', 'Opening hours'),
      array('mon_start', 'Monday start hour'),
      array('mon_end', 'Monday end hour'),
      array('tues_start', 'Tuesday start hour'),
      array('tues_end', 'Tuesday end hour'),
      array('weds_start', 'Wednesday start hour'),
      array('weds_end', 'Wednesday end hour'),
      array('thurs_start', 'Thursday start hour'),
      array('thurs_end', 'Thursday end hour'),
      array('fri_start', 'Friday start hour'),
      array('fri_end', 'Friday end hour'),
      array('sat_start', 'Saturday start hour'),
      array('sat_end', 'Saturday end hour'),
      array('sun_start', 'Sunday start hour'),
      array('sun_end', 'Sunday end hour'),
      array('parking_details', 'Parking details'),
      array('alfresco', 'Alfresco'),
      array('private_function', 'Private function'),
      array('separate_bar', 'Separate bar'),
      array('parking', 'Parking'),
      array('menu_price_band', 'Menu price band'),
      array('images', 'Images'),
      array('description', 'Description'),
      array('livebookings_id', 'Livebookings ID'),
      array('url', 'URL'),
      array('notes', 'Notes'),
    );
    $path = drupal_get_path('module', 'lfsg_locations') . '/csv/locations.csv';

    // Define CSV options.
    $options = array(
      'header_rows' => 1,
      'embedded_newlines' => TRUE,
    );

    // Define extra fields.
    $fields = array(
      'facilities' => 'Facilities',
    );

    // Define source and destination.
    $this->source = new MigrateSourceCSV($path, $columns, $options, $fields);
    $this->destination = new MigrateDestinationNode('location');

    // Define map.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'title' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'Location name',
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Add field mappings.
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('status')
         ->defaultValue(NODE_PUBLISHED);
    $this->addFieldMapping('field_location_livebookings_id', 'livebookings_id');
    $this->addFieldMapping('body', 'description');
    $this->addFieldMapping('body:format')
         ->defaultValue('full_html');
    $this->addFieldMapping('field_location_facilities', 'facilities');
    $this->addFieldMapping('field_location_facilities:create_term')
         ->defaultValue(TRUE);
    $this->addFieldMapping('field_location_fax', 'fax');
    $this->addFieldMapping('field_location_telephone', 'telephone');

    // Unmigrated source fields.
    $this->addUnmigratedSources(array(
      'id',
      'email',
      'address_1',
      'address_2',
      'town',
      'county',
      'postcode',
      'opening_hours',
      'mon_start',
      'mon_end',
      'tues_start',
      'tues_end',
      'weds_start',
      'weds_end',
      'thurs_start',
      'thurs_end',
      'fri_start',
      'fri_end',
      'sat_start',
      'sat_end',
      'sun_start',
      'sun_end',
      'parking_details',
      'alfresco',
      'private_function',
      'separate_bar',
      'parking',
      'menu_price_band',
      'images',
      'url',
      'notes',
    ));

    // Unmigrated destination fields.
    $this->addUnmigratedDestinations(array(
      'uid',
      'created',
      'changed',
      'promote',
      'sticky',
      'revision',
      'log',
      'language',
      'tnid',
      'translate',
      'revision_uid',
      'is_new',
      'body:summary',
      'body:language',
      'field_location_livebookings_id:language',
      'field_location_address',
      'field_location_address:administrative_area',
      'field_location_address:sub_administrative_area',
      'field_location_address:locality',
      'field_location_address:dependent_locality',
      'field_location_address:postal_code',
      'field_location_address:thoroughfare',
      'field_location_address:premise',
      'field_location_address:sub_premise',
      'field_location_address:organisation_name',
      'field_location_address:name_line',
      'field_location_address:first_name',
      'field_location_address:last_name',
      'field_location_address:data',
      'field_location_facilities:source_type',
      'field_location_facilities:ignore_case',
      'field_location_fax:language',
      'field_location_geo',
      'field_location_geo:geo_type',
      'field_location_geo:lat',
      'field_location_geo:lon',
      'field_location_geo:left',
      'field_location_geo:top',
      'field_location_geo:right',
      'field_location_geo:bottom',
      'field_location_geo:geohash',
      'field_location_images',
      'field_location_images:file_class',
      'field_location_images:language',
      'field_location_images:preserve_files',
      'field_location_images:destination_dir',
      'field_location_images:destination_file',
      'field_location_images:file_replace',
      'field_location_images:source_dir',
      'field_location_images:urlencode',
      'field_location_images:alt',
      'field_location_images:title',
      'field_location_opening_hours',
      'field_location_opening_hours:starthours',
      'field_location_opening_hours:endhours',
      'field_location_telephone:language',
      'path',
      'pathauto',
      'comment',
    ));
  }

  /**
   *
   */
  public function prepareRow($row) {

    // Facilities
    $row->facilities = array();

    if ($row->alfresco) {
      $row->facilities[] = 'Alfresco';
    }
    if ($row->private_function) {
      $row->facilities[] = 'Private function';
    }
    if ($row->separate_bar ) {
      $row->facilities[] = 'Separate bar';
    }
    if ($row->parking ) {
      $row->facilities[] = 'Parking';
    }
  }

  /**
   *
   */
  public function prepare($node, stdClass $row) {
    // Address.
    $node->field_location_address[LANGUAGE_NONE][] = array(
      'country' => 'GB',
      'thoroughfare' => $row->address_1,
      'premise' => $row->address_2,
      'locality' => $row->town,
      'administrative_area' => $row->county,
      'postal_code' => $row->postcode,
    );

    // Opening hours.
    foreach(array('sun', 'mon', 'tues', 'weds', 'thurs', 'fri', 'sat') as $index => $day) {
      $start = $day . '_start';
      $end = $day . '_end';

      if (is_numeric($row->$start) && is_numeric($row->$end)) {
        $node->field_location_opening_hours[LANGUAGE_NONE][] = array(
          'day' => $index,
          'starthours' => $row->$start,
          'endhours' => $row->$end,
        );
      }
    }
  }
}
