<?php
/**
 * @file
 * lfsg_vouchers.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function lfsg_vouchers_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
