<?php
/**
 * @file
 * lfsg_vouchers.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function lfsg_vouchers_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_rewards_confirm_details_redirect_path_voucher_reward';
  $strongarm->value = 'rewards/thank-you';
  $export['gk_rewards_confirm_details_redirect_path_voucher_reward'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_rewards_signup_redirect_path_voucher_reward_anonymous';
  $strongarm->value = 'rewards/please-confirm-your-details';
  $export['gk_rewards_signup_redirect_path_voucher_reward_anonymous'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_rewards_signup_redirect_path_voucher_reward_authenticated';
  $strongarm->value = 'user/referrals';
  $export['gk_rewards_signup_redirect_path_voucher_reward_authenticated'] = $strongarm;

  return $export;
}
