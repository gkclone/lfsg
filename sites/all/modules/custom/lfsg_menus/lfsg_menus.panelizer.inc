<?php
/**
 * @file
 * lfsg_menus.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function lfsg_menus_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:menu:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'menu';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_header' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '75da9eee-8c10-478e-8501-3935566095cf';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-a1053ef5-8a22-4085-81ad-cec5c0a0f192';
    $pane->panel = 'content';
    $pane->type = 'block';
    $pane->subtype = 'gk_menus-gk_menus_navigation';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a1053ef5-8a22-4085-81ad-cec5c0a0f192';
    $display->content['new-a1053ef5-8a22-4085-81ad-cec5c0a0f192'] = $pane;
    $display->panels['content'][0] = 'new-a1053ef5-8a22-4085-81ad-cec5c0a0f192';
    $pane = new stdClass();
    $pane->pid = 'new-c21406b6-7bb6-4dea-b1da-9d2febccbe63';
    $pane->panel = 'content';
    $pane->type = 'block';
    $pane->subtype = 'gk_menus-gk_menus_menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c21406b6-7bb6-4dea-b1da-9d2febccbe63';
    $display->content['new-c21406b6-7bb6-4dea-b1da-9d2febccbe63'] = $pane;
    $display->panels['content'][1] = 'new-c21406b6-7bb6-4dea-b1da-9d2febccbe63';
    $pane = new stdClass();
    $pane->pid = 'new-a61baebc-1780-4434-ad4d-8e0b19777503';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a61baebc-1780-4434-ad4d-8e0b19777503';
    $display->content['new-a61baebc-1780-4434-ad4d-8e0b19777503'] = $pane;
    $display->panels['content_header'][0] = 'new-a61baebc-1780-4434-ad4d-8e0b19777503';
    $pane = new stdClass();
    $pane->pid = 'new-158fb501-1c1d-47b8-93eb-1c04bbf6f71b';
    $pane->panel = 'top';
    $pane->type = 'block';
    $pane->subtype = 'lfsg_core-lfsg_core_banner_menus';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '158fb501-1c1d-47b8-93eb-1c04bbf6f71b';
    $display->content['new-158fb501-1c1d-47b8-93eb-1c04bbf6f71b'] = $pane;
    $display->panels['top'][0] = 'new-158fb501-1c1d-47b8-93eb-1c04bbf6f71b';
    $pane = new stdClass();
    $pane->pid = 'new-3b0dde35-f561-4e96-a170-c01058642574';
    $pane->panel = 'top';
    $pane->type = 'block';
    $pane->subtype = 'gk_menus-gk_menus_current_location';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '3b0dde35-f561-4e96-a170-c01058642574';
    $display->content['new-3b0dde35-f561-4e96-a170-c01058642574'] = $pane;
    $display->panels['top'][1] = 'new-3b0dde35-f561-4e96-a170-c01058642574';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:menu:default'] = $panelizer;

  return $export;
}
