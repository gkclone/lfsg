<?php

/**
 * A form for configuring all things Friends Card related.
 */
function lfsg_business_enquiries_admin_config_form() {
  // Facilities.
  $form['facilities'] = array(
    '#type' => 'fieldset',
    '#title' => t('Facilities indicated on enquiry form locations list'),
  );

  $form['facilities']['lfsg_business_enquiries_show_facilities'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Show Facilities'),
    '#options' => lfsg_business_enquiries_get_facilities(),
    '#default_value' => variable_get('lfsg_business_enquiries_show_facilities', 0),
  );

  // Email.
  $form['email'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email configuration'),
  );

  $form['email']['lfsg_business_enquiries_email_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Email domain e.g. "greeneking.co.uk"'),
    '#required' => TRUE,
    '#description' => t('This will be used to generate the location email address by prepending the location "House ID" + "@" + this value'),
    '#default_value' => variable_get('lfsg_business_enquiries_email_domain', 'greeneking.co.uk'),
  );

  $form['email']['lfsg_business_enquiries_default_email_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Default email address'),
    '#required' => TRUE,
    '#description' => t('This is the email address that submissions will be sent to if no "House ID" is specified for a location'),
    '#default_value' => variable_get('lfsg_business_enquiries_default_email_address', 'admin@lfsg.com'),
    '#element_validate' => array('lfsg_business_enquiries_email_element_validate'),
  );

  return system_settings_form($form);
}

/**
 * email validation
 */
function lfsg_business_enquiries_email_element_validate($element, &$form_state, $form) {
   if (!valid_email_address($element['#value'])) {
     form_error($element, t('Please enter a valid email address.'));
   }
}