<?php

/**
 * Check if a Drupal account has a registered Friends Card.
 */
function lfsg_friends_card_api_is_account_registered($account = NULL) {
  // A user is assumed registered if they have the 'friend' role.
  return isset($account) && $account->uid != 0 && in_array('friend', $account->roles);
}

/**
 * Check if a Friends Card has already been registered.
 */
function lfsg_friends_card_api_is_friends_card_registered($friends_card_number) {
  return db_select('field_data_field_user_friends_card_number', 'r')
    ->fields('r', array('field_user_friends_card_number_value'))
    ->condition('field_user_friends_card_number_value', $friends_card_number)
    ->execute()
    ->fetchField();
}

/**
 * Get the ID of the Friends Card reward entity.
 */
function lfsg_friends_card_api_reward_id() {
  return variable_get('lfsg_friends_card_registration_reward');
}

/**
 * Load a Friends Card user details record.
 */
function lfsg_friends_card_api_existing_user_details_load($value, $field = 'udid') {
  if ($user_details = gk_rewards_user_details_load($value, $field)) {
    foreach ($user_details as $record) {
      if (!empty($record->data['friends_card_number'])) {
        return $record;
      }
    }
  }
}
