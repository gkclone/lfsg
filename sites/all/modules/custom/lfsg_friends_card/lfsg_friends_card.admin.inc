<?php

/**
 * A form for configuring all things Friends Card related.
 */
function lfsg_friends_card_admin_config_form() {
  // Rewards.
  $form['rewards'] = array(
    '#type' => 'fieldset',
    '#title' => 'Rewards',
  );

  $options = array(
    0 => t('<None>'),
  );

  foreach (entity_load('reward') as $reward) {
    $options[$reward->rid] = $reward->title;
  }

  $form['rewards']['lfsg_friends_card_registration_reward'] = array(
    '#type' => 'select',
    '#title' => 'Registration reward',
    '#options' => $options,
    '#default_value' => variable_get('lfsg_friends_card_registration_reward', 0),
  );

  $form['rewards']['lfsg_friends_card_welcome_reward'] = array(
    '#type' => 'select',
    '#title' => 'Welcome reward',
    '#options' => $options,
    '#default_value' => variable_get('lfsg_friends_card_welcome_reward', 0),
  );

  // Card number validation.
  $form['card_number_validation'] = array(
    '#type' => 'fieldset',
    '#title' => 'Card number validation',
  );

  $form['card_number_validation']['lfsg_friends_card_number_minimum'] = array(
    '#type' => 'textfield',
    '#title' => 'Minimum',
    '#default_value' => variable_get('lfsg_friends_card_number_minimum', ''),
  );

  $form['card_number_validation']['lfsg_friends_card_number_maximum'] = array(
    '#type' => 'textfield',
    '#title' => 'Maximum',
    '#default_value' => variable_get('lfsg_friends_card_number_maximum', ''),
  );

  $form['card_number_validation']['ignore'] = array(
    '#type' => 'fieldset',
    '#title' => 'Ignore card',
  );

  $form['card_number_validation']['ignore']['lfsg_friends_card_ignore_number'] = array(
    '#type' => 'textfield',
    '#title' => 'Number',
    '#description' => 'This number will be ignored during form validation.',
    '#default_value' => variable_get('lfsg_friends_card_ignore_number', ''),
  );

  $form['card_number_validation']['ignore']['lfsg_friends_card_ignore_message'] = array(
    '#type' => 'textfield',
    '#title' => 'Message',
    '#description' => 'This message will be displayed if the above number is submitted.',
    '#default_value' => variable_get('lfsg_friends_card_ignore_message', ''),
    '#maxlength' => 512,
  );

  return system_settings_form($form);
}
