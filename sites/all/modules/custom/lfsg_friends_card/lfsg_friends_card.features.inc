<?php
/**
 * @file
 * lfsg_friends_card.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function lfsg_friends_card_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_reward_type().
 */
function lfsg_friends_card_default_reward_type() {
  $items = array();
  $items['friends_card_reward'] = entity_import('reward_type', '{
    "type" : "friends_card_reward",
    "label" : "Friends Card",
    "description" : ""
  }');
  return $items;
}
