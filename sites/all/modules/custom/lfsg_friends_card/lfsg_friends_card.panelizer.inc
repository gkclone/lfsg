<?php
/**
 * @file
 * lfsg_friends_card.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function lfsg_friends_card_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:lfsg_friends_card_home';
  $panelizer->title = 'Friends Card: Home';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array(
    'plugins' => array(),
    'logic' => 'and',
  );
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'content_header' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '315a0ddb-8a19-4b48-b12b-1beb32deebfc';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-3cf9ecb6-9052-49b8-8055-44d4471bf1e5';
    $pane->panel = 'content';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'lfsg_friends_card_home',
          'settings' => NULL,
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3cf9ecb6-9052-49b8-8055-44d4471bf1e5';
    $display->content['new-3cf9ecb6-9052-49b8-8055-44d4471bf1e5'] = $pane;
    $display->panels['content'][0] = 'new-3cf9ecb6-9052-49b8-8055-44d4471bf1e5';
    $pane = new stdClass();
    $pane->pid = 'new-f88d4785-dda8-498d-8042-9e9a97079987';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f88d4785-dda8-498d-8042-9e9a97079987';
    $display->content['new-f88d4785-dda8-498d-8042-9e9a97079987'] = $pane;
    $display->panels['content_header'][0] = 'new-f88d4785-dda8-498d-8042-9e9a97079987';
    $pane = new stdClass();
    $pane->pid = 'new-9d3cfedd-c68a-468d-a683-15f7b2863863';
    $pane->panel = 'tertiary';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-secondary-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9d3cfedd-c68a-468d-a683-15f7b2863863';
    $display->content['new-9d3cfedd-c68a-468d-a683-15f7b2863863'] = $pane;
    $display->panels['tertiary'][0] = 'new-9d3cfedd-c68a-468d-a683-15f7b2863863';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:lfsg_friends_card_home'] = $panelizer;

  return $export;
}
