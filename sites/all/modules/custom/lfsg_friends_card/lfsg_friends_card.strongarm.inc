<?php
/**
 * @file
 * lfsg_friends_card.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function lfsg_friends_card_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_rewards_confirm_details_redirect_path_friends_card_reward';
  $strongarm->value = 'friendscard/please-create-account';
  $export['gk_rewards_confirm_details_redirect_path_friends_card_reward'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_rewards_create_account_redirect_path_friends_card_reward';
  $strongarm->value = 'user/friendscard';
  $export['gk_rewards_create_account_redirect_path_friends_card_reward'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_rewards_signup_redirect_path_friends_card_reward_anonymous';
  $strongarm->value = 'friendscard/please-confirm-your-details';
  $export['gk_rewards_signup_redirect_path_friends_card_reward_anonymous'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_rewards_signup_redirect_path_friends_card_reward_authenticated';
  $strongarm->value = 'user/friendscard';
  $export['gk_rewards_signup_redirect_path_friends_card_reward_authenticated'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'lfsg_friends_card_register_redirect_path';
  $strongarm->value = 'friends';
  $export['lfsg_friends_card_register_redirect_path'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'lfsg_friends_card_register_signup_redirect_path';
  $strongarm->value = 'friends/please-confirm-your-existing-details';
  $export['lfsg_friends_card_register_signup_redirect_path'] = $strongarm;

  return $export;
}
