<?php if ($content['content_header'] || $content['content_top'] || $content['content'] || $content['content_secondary'] || $content['content_bottom']): ?>
<div id="main" class="container gk-friends-card">
  <div class="container__inner">
    <div class="grid">
      <div id="primary" class="grid__cell">
        <div class="inner">
          <?php if ($content['content_header']): ?>
          <header id="content-header">
            <?php print render($content['content_header']); ?>
          </header>
          <?php endif; ?>

          <?php if ($content['content']): ?>
          <div id="content" class="grid">
            <?php print render($content['content']); ?>
          </div>
          <?php endif; ?>

          <?php if ($content['find_your_nearest_location']): ?>
          <div class="grid gk-friends-card-find-your-nearest-location">
            <div class="grid__cell desk--one-half grid__cell--choose-location-form-title">
              <div class="box box--grid--gk-friends-card-not-got-card-yet">
                <div class="box__content grid">
                  <?php print render($content['content_secondary']); ?>
                </div>
              </div>
            </div>

            <div class="grid__cell desk--one-half grid__cell--choose-location-form">
              <div class="box box--gk-friends-card-missing-card">
                <div class="box__content grid">
                  <?php print render($content['find_your_nearest_location']); ?>
                </div>
              </div>
            </div>
          </div>
          <?php endif; ?>

          <?php if ($content['content_bottom']): ?>
          <bottom id="content-bottom">
            <div id="content" class="grid">
              <?php print render($content['content_bottom']); ?>
            </div>
          </bottom>
          <?php endif; ?>

        </div>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>