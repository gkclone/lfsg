<?php

// Plugin definition
$plugin = array(
  'title' => t('Friends Card: Public'),
  'category' => t('Page'),
  'icon' => 'lfsg_friendscard_public.png',
  'theme' => 'lfsg_friendscard_public',
  'regions' => array(
    'content_header' => t('Content header'),
    'content' => t('Content'),
    'content_secondary' => t('Secondary content'),
    'find_your_nearest_location' => t('Find your nearest location'),
    'content_bottom' => t('Content bottom'),
  ),
);
