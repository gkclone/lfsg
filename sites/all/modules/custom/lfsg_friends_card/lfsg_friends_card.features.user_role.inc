<?php
/**
 * @file
 * lfsg_friends_card.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function lfsg_friends_card_user_default_roles() {
  $roles = array();

  // Exported role: friend.
  $roles['friend'] = array(
    'name' => 'friend',
    'weight' => 4,
  );

  return $roles;
}
