; GK Distro
includes[gk_distro] = http://code.gkdigital.co/gk_distro/v1.0.6/gk_distro.make

; Site modules
projects[gk_articles][type] = module
projects[gk_articles][subdir] = standard
projects[gk_articles][download][type] = git
projects[gk_articles][download][url] = git@bitbucket.org:greeneking/gk-articles.git
projects[gk_articles][download][tag] = 7.x-1.2

projects[gk_bugherd][type] = module
projects[gk_bugherd][subdir] = standard
projects[gk_bugherd][download][type] = git
projects[gk_bugherd][download][url] = git@bitbucket.org:greeneking/gk-bugherd.git
projects[gk_bugherd][download][tag] = 7.x-1.0

projects[gk_careers][type] = module
projects[gk_careers][subdir] = standard
projects[gk_careers][download][type] = git
projects[gk_careers][download][url] = git@bitbucket.org:greeneking/gk-careers.git
projects[gk_careers][download][tag] = 7.x-1.2

projects[gk_competitions][type] = module
projects[gk_competitions][subdir] = standard
projects[gk_competitions][download][type] = git
projects[gk_competitions][download][url] = git@bitbucket.org:greeneking/gk-competitions.git
projects[gk_competitions][download][tag] = 7.x-1.1

projects[gk_contact][type] = module
projects[gk_contact][subdir] = standard
projects[gk_contact][download][type] = git
projects[gk_contact][download][url] = git@bitbucket.org:greeneking/gk-contact.git
projects[gk_contact][download][tag] = 7.x-1.6

projects[gk_dotmailer][type] = module
projects[gk_dotmailer][subdir] = standard
projects[gk_dotmailer][download][type] = git
projects[gk_dotmailer][download][url] = git@bitbucket.org:greeneking/gk-dotmailer.git
projects[gk_dotmailer][download][tag] = 7.x-1.1

projects[gk_events][type] = module
projects[gk_events][subdir] = standard
projects[gk_events][download][type] = git
projects[gk_events][download][url] = git@bitbucket.org:greeneking/gk-events.git
projects[gk_events][download][tag] = 7.x-1.2

projects[gk_livebookings][type] = module
projects[gk_livebookings][subdir] = standard
projects[gk_livebookings][download][type] = git
projects[gk_livebookings][download][url] = git@bitbucket.org:greeneking/gk-live-bookings.git
projects[gk_livebookings][download][tag] = 7.x-1.2

projects[gk_locations][type] = module
projects[gk_locations][subdir] = standard
projects[gk_locations][download][type] = git
projects[gk_locations][download][url] = git@bitbucket.org:greeneking/gk-locations.git
projects[gk_locations][download][tag] = 7.x-1.16

projects[gk_members][type] = module
projects[gk_members][subdir] = standard
projects[gk_members][download][type] = git
projects[gk_members][download][url] = git@bitbucket.org:greeneking/gk-members.git
projects[gk_members][download][tag] = 7.x-1.5

projects[gk_menus][type] = module
projects[gk_menus][subdir] = standard
projects[gk_menus][download][type] = git
projects[gk_menus][download][url] = git@bitbucket.org:greeneking/gk-menus.git
projects[gk_menus][download][tag] = 7.x-1.4

projects[gk_promotions][type] = module
projects[gk_promotions][subdir] = standard
projects[gk_promotions][download][type] = git
projects[gk_promotions][download][url] = git@bitbucket.org:greeneking/gk-promotions.git
projects[gk_promotions][download][tag] = 7.x-1.1

projects[gk_referrals][type] = module
projects[gk_referrals][subdir] = standard
projects[gk_referrals][download][type] = git
projects[gk_referrals][download][url] = git@bitbucket.org:greeneking/gk-referrals.git
projects[gk_referrals][download][tag] = 7.x-1.5

projects[gk_rewards][type] = module
projects[gk_rewards][subdir] = standard
projects[gk_rewards][download][type] = git
projects[gk_rewards][download][url] = git@bitbucket.org:greeneking/gk-rewards.git
projects[gk_rewards][download][tag] = 7.x-1.20

projects[gk_social][type] = module
projects[gk_social][subdir] = standard
projects[gk_social][download][type] = git
projects[gk_social][download][url] = git@bitbucket.org:greeneking/gk-social.git
projects[gk_social][download][tag] = 7.x-1.1

projects[gk_tiers][type] = module
projects[gk_tiers][subdir] = standard
projects[gk_tiers][download][type] = git
projects[gk_tiers][download][url] = git@bitbucket.org:greeneking/gk-tiers.git
projects[gk_tiers][download][tag] = 7.x-1.3

projects[gk_vouchers][type] = module
projects[gk_vouchers][subdir] = standard
projects[gk_vouchers][download][type] = git
projects[gk_vouchers][download][url] = git@bitbucket.org:greeneking/gk-vouchers.git
projects[gk_vouchers][download][tag] = 7.x-1.5

projects[gk_webform][type] = module
projects[gk_webform][subdir] = standard
projects[gk_webform][download][type] = git
projects[gk_webform][download][url] = git@bitbucket.org:greeneking/gk-webform.git
projects[gk_webform][download][tag] = 7.x-1.4


; Contrib Modules
projects[webform_share][type] = module
projects[webform_share][subdir] = contrib
projects[webform_share][download][type] = get
projects[webform_share][download][url] = http://ftp.drupal.org/files/projects/webform_share-7.x-1.2.tar.gz
projects[webform_share][download][tag] = 7.x-1.2


; Modules to be removed
projects[gk_banners][type] = module
projects[gk_banners][subdir] = standard
projects[gk_banners][download][type] = git
projects[gk_banners][download][url] = git@bitbucket.org:greeneking/gk-banners.git
projects[gk_banners][download][tag] = 7.x-1.0

projects[gk_ctas][type] = module
projects[gk_ctas][subdir] = standard
projects[gk_ctas][download][type] = git
projects[gk_ctas][download][url] = git@bitbucket.org:greeneking/gk-ctas.git
projects[gk_ctas][download][tag] = 7.x-1.0

projects[gk_tripadvisor][type] = module
projects[gk_tripadvisor][subdir] = standard
projects[gk_tripadvisor][download][type] = git
projects[gk_tripadvisor][download][url] = git@bitbucket.org:greeneking/gk-trip-advisor.git
projects[gk_tripadvisor][download][tag] = 7.x-1.0


; Site libraries
projects[naver][type] = library
projects[naver][subdir] = ""
projects[naver][download][type] = git
projects[naver][download][url] = https://github.com/benplum/Naver.git
projects[naver][download][tag] = 3.0.7

projects[jQuery.equalHeights][type] = library
projects[jQuery.equalHeights][subdir] = ""
projects[jQuery.equalHeights][download][type] = git
projects[jQuery.equalHeights][download][url] = https://github.com/mattbanks/jQuery.equalHeights.git
projects[jQuery.equalHeights][download][tag] = v1.5.2

projects[sticky-kit][type] = library
projects[sticky-kit][subdir] = ""
projects[sticky-kit][download][type] = git
projects[sticky-kit][download][url] = https://github.com/leafo/sticky-kit.git
projects[sticky-kit][download][tag] = v1.0.4
